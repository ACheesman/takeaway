const nock = require("nock");

describe("takeaway", function() {
  var takeaway = require("../takeaway.js");

  beforeEach(function() {});

  it("Should have basic functionality", function() {
    nock("https://takeaway.senseinetworks.co.uk")
      .get("/meals-api/v1/burgers")
      .reply(200, [
        {
          patty: "chicken",
          pounds: 3,
          pence: 30,
          name: "test burger",
          description: "test burger description",
          test: "test"
        }
      ]);

    nock("https://takeaway.senseinetworks.co.uk")
      .get("/meals-api/v1/pizzas")
      .reply(200, [
        {
          name: "test pizza",
          pounds: 3,
          pence: 03,
          description: "test pizza description"
        }
      ]);
    takeaway(
      {},
      {
        send: response => {
          //  We expect a response with only price, name, and description
          expect(response).toEqual([
            {
              name: "test pizza",
              price: 3.03,
              description: "test pizza description"
            },
            {
              name: "test burger",
              description: "test burger description",
              price: 3.3
            }
          ]);
          // do nothing
        }
      }
    );
  });
  it("Should still return when one endpoint fails", function() {
    nock("https://takeaway.senseinetworks.co.uk")
      .get("/meals-api/v1/burgers")
      .reply(200, [
        {
          patty: "chicken",
          pounds: 3,
          pence: 30,
          name: "test burger",
          description: "test burger description",
          test: "test"
        }
      ]);

    nock("https://takeaway.senseinetworks.co.uk")
      .get("/meals-api/v1/pizzas")
      .reply(500, [
        {
          name: "test pizza",
          pounds: 3,
          pence: 30,
          description: "test pizza description"
        }
      ]);
    takeaway(
      {},
      {
        send: response => {
          //  We expect a response with only price, name, and description
          expect(response).toEqual([
            {
              name: "test burger",
              description: "test burger description",
              price: 3.3
            }
          ]);
          // do nothing
        }
      }
    );
  });
  it("Should error when both endpoints fail", function() {
    nock("https://takeaway.senseinetworks.co.uk")
      .get("/meals-api/v1/burgers")
      .reply(500, [
        {
          patty: "chicken",
          pounds: 3,
          pence: 30,
          name: "test burger",
          description: "test burger description",
          test: "test"
        }
      ]);

    nock("https://takeaway.senseinetworks.co.uk")
      .get("/meals-api/v1/pizzas")
      .reply(500, [
        {
          name: "test pizza",
          pounds: 3,
          pence: 30,
          description: "test pizza description"
        }
      ]);
    takeaway(
      {},
      {
        status: status => {
          //  We expect a response with only price, name, and description
          expect(status).toEqual(500);
          // do nothing
        },
        send: send => {
          // We're returning 500 and no body content
        }
      }
    );
  });
  it("Should sort burgers correctly", function() {
    nock("https://takeaway.senseinetworks.co.uk")
      .get("/meals-api/v1/burgers")
      .reply(200, [
        {
          patty: "chicken",
          pounds: 3,
          pence: 30,
          name: "test chicken burger",
          description: "test burger description",
          test: "test"
        },
        {
          patty: "beef",
          pounds: 3,
          pence: 30,
          name: "beef burger",
          description: "test beef burger description",
          test: "test"
        },
        {
          patty: "veggie",
          pounds: 3,
          pence: 30,
          name: "test veggie burger",
          description: "test veggie burger description",
          test: "test"
        },
        {
          patty: "chicken",
          pounds: 2,
          pence: 02,
          name: "test cheap chicken burger",
          description: "test cheap chicken burger description",
          test: "test"
        }
      ]);

    nock("https://takeaway.senseinetworks.co.uk")
      .get("/meals-api/v1/pizzas")
      .reply(200, [
        {
          name: "test pizza",
          pounds: 3,
          pence: 03,
          description: "test pizza description"
        }
      ]);
    takeaway(
      {},
      {
        send: response => {
          //  We expect a response with only price, name, and description
          expect(response).toEqual([
            {
              name: "test pizza",
              price: 3.03,
              description: "test pizza description"
            },
            {
              price: 2.02,
              name: "test cheap chicken burger",
              description: "test cheap chicken burger description"
            },
            {
              price: 3.3,
              name: "test chicken burger",
              description: "test burger description"
            },
            {
              price: 3.3,
              name: "beef burger",
              description: "test beef burger description"
            },
            {
              price: 3.3,
              name: "test veggie burger",
              description: "test veggie burger description"
            }
          ]);
          // do nothing
        }
      }
    );
  });
  it("Should have sort pizzas correctly", function() {
    nock("https://takeaway.senseinetworks.co.uk")
      .get("/meals-api/v1/burgers")
      .reply(200, [
        {
          patty: "chicken",
          pounds: 3,
          pence: 30,
          name: "test burger",
          description: "test burger description",
          test: "test"
        }
      ]);

    nock("https://takeaway.senseinetworks.co.uk")
      .get("/meals-api/v1/pizzas")
      .reply(200, [
        {
          name: "test pizza 703",
          pounds: 7,
          pence: 03,
          description: "test pizza description"
        },
        {
          name: "test pizza 309",
          pounds: 3,
          pence: 09,
          description: "test pizza description"
        },
        {
          name: "test pizza 102",
          pounds: 1,
          pence: 02,
          description: "test pizza description"
        },
      ]);
    takeaway(
      {},
      {
        send: response => {
          //  We expect a response with only price, name, and description
          expect(response).toEqual([
            {
              name: "test pizza 102",
              price: 1.02,
              description: "test pizza description"
            },
            {
              name: "test pizza 309",
              price: 3.09,
              description: "test pizza description"
            },
            {
              name: "test pizza 703",
              price: 7.03,
              description: "test pizza description"
            },
            {
              name: "test burger",
              description: "test burger description",
              price: 3.30
            }
          ]);
          // do nothing
        }
      }
    );
  });
});
