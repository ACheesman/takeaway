// Node.js takeaway app
// Anthony Cheesman

// Fetches takeaway from two endpoints, sorts, filters, and flattens the result
// Error handling could be better (ie: specific)

const express = require("express");
const https = require("https");
const app = express();
const port = 3000;

app.get("/", takeawayResponse);

app.listen(port, () => console.log(`Takeaway app listening on port ${port}!`));

function takeaway(food) {
  return new Promise(function(resolve, reject) {
    let url = "https://takeaway.senseinetworks.co.uk/meals-api/v1/" + food;
    https.get(url, res => {
      res.setEncoding("utf8");
      let body = "";
      res.on("data", data => {
        body += data;
      });
      res.on("end", () => {
        if (res.statusCode !== 200) {
          reject();
        } else {
          try{
            body = JSON.parse(body);
          }
          catch(err){
            reject();
          }


          if (food === "burgers") {
            body.sort(burgerSort);
          }

          let menu = body.map(item => {
            normalisedItem = {};
            normalisedItem.name = item.name;
            normalisedItem.description = item.description;
            normalisedItem.price = item.pounds + item.pence / 100;
            return normalisedItem;
          });
          if (food === "pizzas") {
            menu.sort(pizzaSort);
          }
          resolve(menu);
        }
      });
    });
  });
}

function pizzaSort(a, b) {
  return a.price - b.price;
}
function burgerSort(a, b) {
  if (pattySort(a.patty) != pattySort(b.patty)) {
    return pattySort(b.patty) - pattySort(a.patty);
  } else {
    if (a.pounds === b.pounds) {
      return a.pence - b.pence;
    } else {
      return a.pounds - b.pounds;
    }
  }
}

function pattySort(patty) {
  if (patty === "chicken") {
    return 3;
  } else if (patty === "beef") {
    return 2;
  } else {
    return 1;
  }
}

function takeawayResponse(req, res) {
  let foodData = [];
  let pizzaData = takeaway("pizzas");
  let burgerData = takeaway("burgers");
  let endpointFailures = 0;
  Promise.all([
    pizzaData.catch(error => {
      endpointFailures++;
      return error;
    }),
    burgerData.catch(error => {
      endpointFailures++;
      return error;
    })
  ]).then(
    foods => {
      if (endpointFailures > 1) {
        res.status(500);
        res.send;
      }
      res.send(foods.filter(menu => menu).flat());
    },
    err => {
      res.status(500);
      res.send;
    }
  );
}

module.exports = takeawayResponse;
